local functions = require('functions')

local pp_number = arg[1]

while true do
   if not functions.is_pp_connected(pp_number) then
      rt.syslog('info', string.format('PP[%02d] is disconnected. initiate reconnecting.', pp_number))
      functions.pp_connect(pp_number)
   end

   rt.sleep(30)
end
