describe('functions', function()
   setup(function()
      _G.rt = {}
   end)

   insulate('is_pp_connected', function()
      local functions = require('functions')

      it('returns true when pp is connected', function()
         rt.command = function()
            return true, [[
PP[01]:
Current PPTP session status is Connected.
Peer Hostname: local
5 hours 28 minutes 4 seconds  connection.
Received: 1983 packets [112111 octets]
Transmitted: 1980 packets [125034 octets]
PPP Configure Options
    LCP Local: Magic-Number MRU, Remote: MSCHAP-V2 Magic-Number
    IPCP Local:, Remote:
    PP IP Address Local: Unnumbered, Remote: Unnumbered
    IPV6CP Local: Interface-ID, Remote: Interface-ID
    PP Interface-ID Local: 02a0defffe000000, Remote: 0000000000000001
    CCP: Mppe-128bit]]
         end
         spy.on(rt, 'command')

         assert.is_true(functions.is_pp_connected(1))
         assert.stub(rt.command).was.called_with('show status pp 1')
      end)

      it('returns false when pp is disconnected', function()
         rt.command = function()
            return true, [[
PP[01]:
Current PPTP session status is Offline.
Last PPTP session status:
Peer Hostname: local
        from 2020/06/01 15:27:01 to 2020/06/01 20:55:16,
5 hours 28 minutes 15 seconds  connection.
Disconnected cause is [No error.].]]
         end
         spy.on(rt, 'command')

         assert.is_false(functions.is_pp_connected(1))
         assert.stub(rt.command).was.called_with('show status pp 1')
      end)
   end)

   insulate('pp_connect', function()
      local functions = require('functions')

      it('executes desired commands', function()
         rt.command = function() end
         spy.on(rt, 'command')

         functions.pp_connect(1)
         assert.stub(rt.command).was.called_with('connect pp 1')
      end)
   end)
end)
