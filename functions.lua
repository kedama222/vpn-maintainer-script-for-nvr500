local functions = {}

function functions.is_pp_connected(pp_number)
   local rt_command
   local rt_command_output
   local rt_command_result
   local status

   rt_command = string.format('show status pp %d', pp_number)
   rt_command_result, rt_command_output = rt.command(rt_command)
   if rt_command_result then
      status = string.match(rt_command_output, 'status is ([^.]+)%.')
      return status == 'Connected'
   end
end

function functions.pp_connect(pp_number)
   rt.command(string.format('connect pp %d', pp_number))
end

return functions
