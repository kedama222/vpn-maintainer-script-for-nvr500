exclude_files = {
   '.luarocks/*'
}

read_globals = {
   rt = {
      fields = {
         command = {},
         sleep = {},
         syslog = {}
      }
   }
}

files['spec/*_spec.lua'] = {
   read_globals = {
      assert = {
         fields = {
            is_true = {},
            stub = {}
         }
      },
      describe = {},
      it = {},
      stub = {},
      spy = {
         fields = {
            on = {}
         }
      }
   },
   globals = {
      rt = {
         fields = {
            command = {}
         }
      }
   }
}
